import { Component, OnInit } from '@angular/core';
import  { FarmManagementService } from './farm-management.service';
import {Router} from "@angular/router";
import{config} from '..//app.config';


@Component({
  selector: 'app-farm-management',
  templateUrl: './farm-management.component.html',
  styleUrls: ['./farm-management.component.scss']
})
export class FarmManagementComponent implements OnInit {
  error;
  newFarmList: any;
  approvedFarmList: any;
  disapprovedFarmList: any;
  imageUrl = config.getDownloadUrl();

  currentPageNew = 1;
  totalElementsNew;

  currentPageApprove = 1;
  totalElementsApprove;

  currentPageDisapprove = 1;
  totalElementsDisapprove;


  constructor(private farmService: FarmManagementService, private router: Router) {
console.log(this.imageUrl);
  }

  viewFarm(id) {
  this.router.navigate(['/farm-detail', id]);
}

  ngOnInit() {
    this.getNewFarm();
    this.getApprovedFarm();
    this.getDispprovedFarm();

  }

  acceptFarm(id){
    this.farmService.acceptNewFarm('APPROVED', id).map(response =>
      response.json()).subscribe(response => {
        this.getDispprovedFarm();
        this.getNewFarm();
        this.getApprovedFarm();
      },
      error => {
        this.error = JSON.parse(error._body);

        console.log(this.error.description);
      }),
      () => console.log('completed');
  }
   rejectFarm(id){
    this.farmService.acceptNewFarm('DISAPPROVED', id).map(response =>
      response.json()).subscribe(response => {
        this.getDispprovedFarm();
        this.getApprovedFarm();
        this.getNewFarm();
      },
      error => {
        this.error = JSON.parse(error._body);

        console.log(this.error.description);
      }),
      () => console.log('completed');
  }


  getNewFarm() {
    this.farmService.getFarm('NEW', this.currentPageNew - 1).map(response =>
      response.json()).subscribe(response => {
        this.totalElementsNew = response.totalElements;
        this.newFarmList = response.content;
        console.log(this.newFarmList);
      },
      error => {
        this.error = JSON.parse(error._body);

        console.log(this.error.description);
      }),
      () => console.log('completed');
  }

  getDispprovedFarm() {
    this.farmService.getFarm('DISAPPROVED',this.currentPageDisapprove - 1).map(response =>
      response.json()).subscribe(response => {
        this.totalElementsDisapprove = response.totalElements;
        this.disapprovedFarmList = response.content;
        console.log(this.disapprovedFarmList);
      },
      error => {
        this.error = JSON.parse(error._body);

        console.log(this.error.description);
      }),
      () => console.log('completed');
  }

  getApprovedFarm() {
    this.farmService.getFarm('APPROVED' , this.currentPageApprove - 1).map(response =>
      response.json()).subscribe(response => {
        this.totalElementsApprove = response.totalElements;
        this.approvedFarmList = response.content;
        console.log(this.approvedFarmList);
      },
      error => {
        this.error = JSON.parse(error._body);

        console.log(this.error.description);
      }),
      () => console.log('completed');
  }

  pageChangedNew(){
    this.getNewFarm();
  }

  pageChangedApprove(){
    this.getApprovedFarm();
  }

  pageChangedDisapprove(){
    this.getDispprovedFarm();
  }

}
