import {Http, Response, Headers} from '@angular/http';
import{config} from '..//app.config';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';

@Injectable()
export class FarmManagementService {

  fakeData;
  private farmUrl = config.getEnvironmentVariable('endPoint') + 'api/v1/farmProfile/';
  private acceptUrl = config.getEnvironmentVariable('endPoint') + 'api/v1/farmProfile/';
  constructor(private http: Http) {
  }

  getFarm(status, page){
    return this.http.get(this.farmUrl + status +'/page', {headers: config.getJSONHeader()});
  }

  acceptNewFarm(status, id){
    return this.http.put(this.acceptUrl + id + '/' + status,this.fakeData, {headers: config.getJSONHeader()});
  }

}



