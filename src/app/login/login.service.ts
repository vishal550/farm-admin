import {Http, Response, Headers} from '@angular/http';
import{config} from '..//app.config';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

  private loginUrl = config.getEnvironmentVariable('endPoint') + 'api/v1/authenticate';
  constructor(private http: Http) {
  }

  login(data) {
    let headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post(this.loginUrl,data, {headers: headers});


  }

}



