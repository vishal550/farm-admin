import { Component, OnInit } from '@angular/core';
import { Login} from './login.modal';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginModal = new Login;
  error;
  constructor(private loginService:LoginService,private router:Router) {
    console.log(this.loginModal);
  }
  login(){
    console.log(this.loginModal);
    this.loginService.login(this.loginModal).map(response => response.json()).subscribe(response =>{
        localStorage.setItem('farm_token', response.id_token);
        this.router.navigate(['/homepage']);
        },error => {
        console.log(error._body);
        this.error = JSON.parse(error._body);
        console.log(this.error.message);
      })
  }
  ngOnInit() {
  }

}




