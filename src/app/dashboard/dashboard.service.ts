import {Http, RequestOptions, URLSearchParams} from '@angular/http';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';
import {config} from "../app.config";

@Injectable()
export class UserService {

  private userUrl = config.getEnvironmentVariable('endPoint') + 'api/v1/userProfile/user/paged';
  constructor(private http: Http) {
  }

  getUser(currentPage) {
    let params = new URLSearchParams();
    params.append('page', currentPage);
    params.append('size', '10');
    let options = new RequestOptions({ headers: config.getJSONHeader(), search: params });
    return this.http.get(this.userUrl, options);
  }

}
