import { Component, OnInit } from '@angular/core';
import  { UserService } from './dashboard.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  error:any;
  userlist:any;
  currentPage = 1;
  totalElements;

  constructor(private userService:UserService) {
    this.getUser();
  }
    getUser(){
      this.userService.getUser(this.currentPage - 1).map(response =>
        response.json()).subscribe(response => {
          this.totalElements = response.totalElements;
          this.userlist = response.content;
          console.log(this.userlist);
        },
        error => {
          this.error = JSON.parse(error._body);

          console.log(this.error.description);
        }),
        () => console.log('completed');
    }


  public disabled:boolean = false;
  public status:{isopen:boolean} = {isopen: false};

  public toggled(open:boolean):void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event:MouseEvent):void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit(): void {}

  pageChanged(){
    this.getUser();
  }
}
