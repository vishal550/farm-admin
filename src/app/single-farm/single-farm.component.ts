import { Component, OnInit } from '@angular/core';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import { Lightbox } from 'angular2-lightbox';
import {ActivatedRoute, Router} from '@angular/router';
import { ViewFarmService } from './single-farm.service'
import{config} from '..//app.config';

@Component({
  selector: 'app-single-farm',
  templateUrl: './single-farm.component.html',
  styleUrls: ['./single-farm.component.scss'],
  providers: [NgbRatingConfig]
})
export class SingleFarmComponent {
  error;
  readonly = false;
  private album = [];
  public albums = [];
  public farmId;
  public singleFarm;
  public accomodationList;
  public imageGallery = [];
  public images = [];
  public reviewList;
  imageUrl = config.getDownloadUrl();


  constructor(private viewFarmService: ViewFarmService, private lightbox: Lightbox, private route: ActivatedRoute, private router: Router, config2: NgbRatingConfig) {
    this.farmId = route.snapshot.params['id'];
      config2.max = 5;
      config2.readonly = true;

    if(this.farmId){
      this.viewFarmService.getSingleFarm(this.farmId).map(response =>
        response.json()).subscribe(response => {
          this.singleFarm = response;
          this.accomodationList = response.accommodations;
          this.images = response.images;
          for (let i = 0; i < this.images.length; i++) {
            const album ={
              src: this.imageUrl + this.images[i].image,
              thumb: this.imageUrl + 'thumb_' + this.images[i].image,
            }
            this.imageGallery.push(album);
          }
          console.log(this.imageGallery);
          console.log(this.singleFarm);
          console.log( this.imageGallery);
        },
        error => {
          this.error = JSON.parse(error._body);

          console.log(this.error.description);
        }),
        () => console.log('completed');

      this.getReview();

    }
  }
  open(index: number): void {
    // open lightbox
    this.lightbox.open(this.imageGallery, index);
  }


  getReview(){
    this.viewFarmService.getReview(this.farmId).map(response =>
      response.json()).subscribe(response => {
        this.reviewList = response.content;
        console.log(this.reviewList);
      },
      error => {
        this.error = JSON.parse(error._body);

        console.log(this.error.description);
      }),
      () => console.log('completed');
  }
}



