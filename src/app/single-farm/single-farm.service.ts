import {Http, Response, Headers} from '@angular/http';
import{config} from '..//app.config';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';

@Injectable()
export class ViewFarmService {

  private singleFarmUrl = config.getEnvironmentVariable('endPoint') + 'api/v1/farmProfile/';
  private reviewUrl = config.getEnvironmentVariable('endPoint') + 'api/v1/rating/';

  constructor(private http: Http) {
  }

  getSingleFarm(id){
    return this.http.get(this.singleFarmUrl + id, {headers: config.getJSONHeader()});
  }

  getReview(id){
    return this.http.get(this.reviewUrl + id + '/page', {headers: config.getJSONHeader()});
  }

}



