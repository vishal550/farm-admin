import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//Layouts
import { DashboardComponent } from './dashboard/dashboard.component';
import { FarmManagementComponent } from './farm-management/farm-management.component';
import { LoginComponent } from './login/login.component';
import { SingleFarmComponent } from './single-farm/single-farm.component';
import { LogoutComponent } from './logout/logout.component';
import { AccomdationComponent } from './accomdation/accomdation.component';


export const routes: Routes = [
  {
    path: '',
    redirectTo: 'app-login',
    pathMatch: 'full',
  },
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'homepage',
    component: DashboardComponent,
    data: {
      title: 'Home'
    }
  },
  {
    path: 'farm-management',
    component: FarmManagementComponent,
    data: {
      title: 'Farm Management'
    }
  },
  {
    path: 'farm-detail/:id',
    component: SingleFarmComponent,
    data: {
      title: 'Single Farm Detail'
    }
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'farm-accomdation',
    component: AccomdationComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
