import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StarRatingModule } from 'angular-star-rating';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { LightboxModule } from 'angular2-lightbox';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ng2-bootstrap/dropdown';
import { TabsModule } from 'ng2-bootstrap/tabs';


import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// Routing Module
import { AppRoutingModule } from './app.routing';
import { HttpModule } from '@angular/http';


//Layouts
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import {LoginService} from "./login/login.service";
import {UserService} from "./dashboard/dashboard.service";
import {FarmManagementService} from "./farm-management/farm-management.service";
import { FarmManagementComponent } from './farm-management/farm-management.component';
import { SingleFarmComponent } from './single-farm/single-farm.component';
import { ViewFarmService } from './single-farm/single-farm.service';
import { TopbarService } from './header/header.service';
import { AccomdationComponent } from './accomdation/accomdation.component';
import { AccomdationService } from './accomdation/accomdation.service';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    NgbModule.forRoot(),
    StarRatingModule,
    LightboxModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    LoginComponent,
    FarmManagementComponent,
    SingleFarmComponent,
    LogoutComponent,
    AccomdationComponent
  ],
  providers: [
    LoginService,
    UserService,
    FarmManagementService,
    ViewFarmService,
    TopbarService,
    AccomdationService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
