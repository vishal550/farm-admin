import {Http, RequestOptions,Headers, URLSearchParams} from '@angular/http';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';
import {config} from "../app.config";

@Injectable()
export class AccomdationService {

  private accomdationUrl = config.getEnvironmentVariable('endPoint') + 'api/v1/accommodation/page';
  private postImageUrl =  config.getEnvironmentVariable('endPoint') + 'api/v1/upload/image';
  private addAccomdationUrl =  config.getEnvironmentVariable('endPoint') + 'api/v1/accommodation';
  constructor(private http: Http) {
  }

  getAccomdation(currentPage) {
    let params = new URLSearchParams();
    params.append('page', currentPage);
    params.append('size', '10');
    let options = new RequestOptions({ headers: config.getJSONHeader(), search: params });
    return this.http.get(this.accomdationUrl, options);
  }
  addAccomdation(data) {
    return this.http.post(this.addAccomdationUrl,data, { headers: config.getJSONHeader()});
  }
  editAccomdation(data) {
    return this.http.put(this.addAccomdationUrl,data, { headers: config.getJSONHeader()});
  }
  uploadImage(image){
    let jwt = localStorage.getItem('farm_token');
    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' + jwt);
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.postImageUrl,image, options);

  }

}
