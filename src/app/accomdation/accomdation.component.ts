import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {AccomdationService} from "./accomdation.service";
import {config} from '../app.config';

@Component({
  selector: 'app-accomdation',
  templateUrl: './accomdation.component.html',
  styleUrls: ['./accomdation.component.scss']
})
export class AccomdationComponent implements OnInit {
  closeResult: string;
  modalReference: any;
  image = 'assets/images/upload.jpg';
  photo;
  name;
  error;
  editAccomdationModal = false;
  totalElements;
  imageUrl = config.getDownloadUrl();
  accomdationList :any;
  currentPage = 1;
  accomdationModel = new AccomdationModel;

  constructor(private modalService: NgbModal,public accomdationService : AccomdationService) {
    this.getAccomdation();
    console.log(this.imageUrl);
  }
  getAccomdation(){
    this.accomdationService.getAccomdation(this.currentPage - 1).map(response =>
      response.json()).subscribe(response => {
        this.totalElements = response.totalElements;
        this.accomdationList = response.content;
        console.log(this.accomdationList);
      },
      error => {
        this.error = JSON.parse(error._body);

        console.log(this.error.description);
      }),
      () => console.log('completed');
  }

  ngOnInit() {
  }
  open(content) {


    //this.modalReference = this.modalService.open(content);
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  fileChange(event) {
    let fileList: FileList = event.target.files;
    // let file    = document.querySelector('img');
    let reader = new FileReader();
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData();
      formData.append('file', file, file.name);

      reader.onload = (event) => {
        this.image = reader.result;
      };

      if (file) {
        reader.readAsDataURL(file);
      } else {
      }
      this.accomdationService.uploadImage(formData).map(response =>response.json()).subscribe(
        data => {
          this.accomdationModel.image = data.image;

        },
        error => console.log(error)
      )
    }
  }
  addAccomdation() {
    if (!this.editAccomdationModal) {
      this.accomdationService.addAccomdation(this.accomdationModel).map(response => response.json()).subscribe(
        data => {
          window.location.reload();

        },
        error => console.log(error)
      )
    }
    else{
      this.accomdationService.editAccomdation(this.accomdationModel).map(response => response.json()).subscribe(
        data => {
          // window.location.reload();

        },
        error => console.log(error)
      )
    }
  }
  editAccomdation(content,accomdation){
   this.open(content);
    this.editAccomdationModal = true;
    this.accomdationModel.name= accomdation.name;
    this.accomdationModel.id= accomdation.iid;
    this.accomdationModel.image= accomdation.image;
    this.image = this.imageUrl + accomdation.image;
  }
}

export class AccomdationModel {
  'image':string;
  'name':string;
  'id':string;
}
