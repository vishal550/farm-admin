import {Http, Response, Headers} from '@angular/http';
import{config} from '..//app.config';
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';

@Injectable()
export class TopbarService {

  private changePasswordUrl = config.getEnvironmentVariable('endPoint') + 'api/v1/account/change_password' ;
  private headers = config.getJSONHeader();
  constructor(private http: Http) {
  }

  changePassword(data) {
    return this.http.post(this.changePasswordUrl , data , {headers: this.headers});
  }

}



