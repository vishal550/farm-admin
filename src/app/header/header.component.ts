import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ChangePassword} from './header.model';
import {Router} from '@angular/router';
import {TopbarService} from './header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  closeResult: string;
  modalReference: any;
  error;
  public changePasswordModel = new ChangePassword;
  constructor(private router:Router,private modalService: NgbModal, private topbarService:TopbarService) { }

  ngOnInit() {
  }
  open(content) {


    //this.modalReference = this.modalService.open(content);
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  changePassword(){
    this.topbarService.changePassword(this.changePasswordModel).map(response =>
      response.json()).subscribe(response => {
        localStorage.removeItem('farm_token');
        this.router.navigate(['']);
        window.location.reload();
      },
      error => {
        this.error = JSON.parse(error._body);

        console.log(this.error.description);
      }),
      () => console.log('completed');
  }
}
